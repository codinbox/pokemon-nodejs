var express = require('express');
app = express();

port = process.env.PORT ||8080;
var Users = require('./model');
var routes = require('./routes.js');
app.use(express.static(__dirname + '/public'));
routes(app);
app.listen(port);