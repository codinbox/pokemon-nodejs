'use strict';

var mongoose = require('mongoose'),
Deck = mongoose.model('Deck');

var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

exports.save_deck = function(req, res) {
	Deck.update({ _id : req.params.player }, { $set : { cards : req.body.cards } }, { upsert : true }, function(err, deck) {
		if (err)
			res.send(err);
		res.json(deck);
	})
};

exports.get_deck = function(req, res) {
	Deck.find({_id: req.params.player}, function(err, deck) {
		if (err)
			res.send(err);
		res.json(deck);
	});
};
