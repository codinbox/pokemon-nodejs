'use strict';
module.exports = function (app) {
	var controller = require('./controller');

	app.route('/deck/:player')
		.get(controller.get_deck)
		.put(controller.save_deck);

	app.get('/', function(req, res) {
		res.sendFile('public/index.html', { root: __dirname }); // load the single view file (angular will handle the page changes on the front-end)
	});
}