'use strict';
var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://codinbox-pokemon:swuSacu5$mac@ds135196.mlab.com:35196/codinbox-pokemon',{useMongoClient: true});

var Schema = mongoose.Schema;
var DeckSchema = new Schema({
	_id: {
		type: String,
		required: 'Nom du joueur'
	},
	cards: []
});
module.exports = mongoose.model('Deck', DeckSchema);
